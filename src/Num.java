import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Num {
    private Num() {}

    public static int[] numsFromFile(String filename) throws IOException, InvalidNumberException {
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        String line = reader.readLine().replaceAll(" ", "");
        String[] chunks = line.split(",");
        if (chunks.length != 10) {
            throw new InvalidNumberException("the amount of numbers in the file must be 10");
        }

        int[] nums = new int[10];

        for (int i = 0; i < chunks.length; i++) {
            try {
                nums[i] = Integer.parseInt(chunks[i]);
                if (nums[i] < 0) {
                    throw new InvalidNumberException("negative numbers aren't allowed");
                }
            } catch (NumberFormatException e) {
                throw new InvalidNumberException("the number at index " + i + " is not a valid number");
            }
        }

        return nums;
    }

    public static int sum(int[] nums) {
        int sum = 0;

        for (int num : nums) {
            sum += num;
        }

        return sum;
    }
}
