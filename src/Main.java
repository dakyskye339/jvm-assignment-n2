import java.io.IOException;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        try {
            int[] nums = Num.numsFromFile("numbers.txt");
            System.out.printf("the numbers are %s\n", Arrays.toString(nums));
            int sum = Num.sum(nums);
            System.out.printf("the sum is %d\n", sum);
        } catch (InvalidNumberException | IOException e) {
            System.out.println(e.getMessage());
        }
    }
}